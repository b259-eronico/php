<?php
require_once "./code.php";
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>s1 activity1</title>
</head>
<body>

<h3>Activity-1</h3>
<h1>Full Address</h2>
<p> <?php echo getFullAddress('Philippines', 'Cebu', 'Minglanilla', 'Block 01 Lot 13, Easy Access Subdv.'); ?> </p>

<h3>Activity-2</h3>
<h1>Grades</h2>
<p> <?php echo getLetterGrade(78); ?> </p>
<p> <?php echo getLetterGrade(88); ?> </p>
<p> <?php echo getLetterGrade(98); ?> </p>
<p> <?php echo getLetterGrade(101); ?> </p>

</body>
</html>