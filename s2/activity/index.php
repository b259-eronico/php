<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>s2 Activity</title>
</head>
<body>
        
    <h4>Activity - 1 </h4>

    <h1>Loop</h1>

    <?php printDivisibleOfFive(); ?> 

    <h4>Activity - 2 </h4>

    <h1> Array Manipulation</h1>

    <?php array_push($studentsArray, 'Iron Man'); ?>
    <?php array_push($studentsArray, 'Thor'); ?>
    <?php array_push($studentsArray, 'Hulk'); ?>

    <pre> <?php print_r($studentsArray); ?> </pre>

    <h4>count</h4>
    <pre> <?php echo count($studentsArray); ?> </pre>

    <?php array_unshift($studentsArray, 'Loki'); ?>
    <pre> <?php print_r($studentsArray); ?> </pre>
    <h4>count</h4>
    <pre> <?php echo count($studentsArray); ?> </pre>

    <?php array_shift($studentsArray); ?>
    <pre> <?php print_r($studentsArray); ?> </pre>
    <h4>count </h4>
    <pre> <?php echo count($studentsArray); ?> </pre>

</body>
</html>