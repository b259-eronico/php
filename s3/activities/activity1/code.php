<?php

class Person {

        public $firstName;
        public $middleName;
        public $lastName;

        public function __construct($firstName, $middleName, $lastName) {
            $this->firstName = $firstName;
            $this->middleName = $middleName;
            $this->lastName = $lastName;
        }

}

class Developer extends Person {

}

class Engineer extends Person {

}



$person = new Person ('Senku', 'watitiw', 'Ishingami.');

$developer = new Developer ('John', 'Finch' , 'Smith');

$engineer = new Engineer ('Harold', 'Myers' , 'Reese.');