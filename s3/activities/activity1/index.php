<?php require_once './code.php' ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>s3 activity-1</title>
</head>
<body>

    <h1>Person</h1>
    <p> Your full name is <?php echo $person->firstName, ' ', $person->lastName; ?> </p>

    <h1>Developer</h1>
    <p> Your  name is <?php echo $developer->firstName, ' ', $developer->middleName, ' ',$developer->lastName; ?> and you are a developer. </p>

    <h1>Engineer</h1>
    <p> You are an engineer named <?php echo $engineer->firstName, ' ',$engineer->middleName, ' ', $engineer->lastName; ?> </p>

    
</body>
</html>