<?php

// $_GET and $_POST are "super global varriables" in PHP.
// These let you retrieve information sent by the client.

// "super global" variable allows data to persist between pages or a single session.
// it is always accessible regardless the scope.

// Both $_Get and $_Post creates an associative array that holds key=>value pairs;
    // "Key" represent the name of the form control/input element.
    // "value" represents the user input.

// var_dump($_GET);
// var_dump($_POST);

$tasks = ["Get git", "Bake HTML", "Eat CSS", "Learn PHP"];

if(isset($_GET["index"])){
    $indexGet = $_GET["index"];
echo "The retrieved task from GET is $tasks[$indexGet] <br/>";
}


if(isset($_POST["index"])){
    $indexPost = $_POST["index"];
    echo "The retrieved task from POST is $tasks[$indexPost] <br/>";
}




?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S04: client-Server Communication (GET and POST)</title>
</head>
<body>
    
    <h1>Task index form GET</h1>

    <form method="GET">
        <!-- Get METHOD  is used for non-sensitive data or for sorting/filtering-->
        <select name="index" required>
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
        </select>
        <button type="submit">GET</button>
        <!-- <label>Email:</label>
        <input type="email" name="email">
        <br>
        <label>Password:</label>
        <input type="password" name="password">
        <br>
        <button type="submit">Login</button> -->
    </form>

    <h1>Task index form POST</h1>

        <!-- POST Method is used for sending sensitive information -->
    <form method="POST">
        <select name="index" required>
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
        </select>
        <button type="submit">POST</button>
    </form>
</body>
</html>