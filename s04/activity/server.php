<br>
<br>
<br>
<br>
<?php
session_start();

if (isset($_SESSION['username'])) {
    echo "Hey, {$_SESSION['username']} <br>";
    echo '<form method="post" action=""><br><input type="submit" name="logout" value="Logout"></form>';
} else {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $username = $_POST['username'];
        $password = $_POST['password'];

        if ($username === 'johnsmith@gmail.com' && $password === '1234') {
            $_SESSION['username'] = $username;

            header('Location: server.php');
            exit;
        } else {
            echo 'Incorrect username or password';
        }
    }
}


if (isset($_POST['logout'])) {
    session_destroy();
    header('Location: index.php');
    exit;
}
